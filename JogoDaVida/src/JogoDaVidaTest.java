import static org.junit.Assert.*;

import org.junit.Test;

public class JogoDaVidaTest {

	@Test
	public void verificaPosicao1e2Matriztest() {
		assertEquals("X", JogoDaVida.verificarPosicao(1, 2));
	}
	
	public void verificaPosicao2e3Matriztest() {
		assertEquals("X", JogoDaVida.verificarPosicao(2, 3));
	}
	
	public void verificaPosicao3e1Matriztest() {
		assertEquals("X", JogoDaVida.verificarPosicao(3, 1));
	}
	
	public void verificaPosicao1e1Matriztest() {
		assertEquals(" ", JogoDaVida.verificarPosicao(1, 1));
	}

}
