import java.util.HashMap;

public class JogoDaVida {

	public static void main(String[] args) {
		
	}
	
	public static String verificarPosicao(int pos1, int pos2) {
		HashMap<Integer, Integer> posicoes = new HashMap<Integer, Integer>();
		posicoes.put(1, 2);
		posicoes.put(2, 3);
		posicoes.put(3, 1);
		String [][] matriz = imprimirMatriz(posicoes);
		
		return matriz[pos1][pos2];
	}
	
	public static String[][] imprimirMatriz(HashMap<Integer, Integer> posicoes){
		
		String[][] matriz = new String[5][5];
		
		for(int i= 0; i < 5;i++){
			for(int j= 0; j < 5;j++){
				if (posicoes.containsKey(i) && posicoes.get(i) == j){
					matriz[i][j] = "X";				
				}
				else
				matriz[i][j] = " ";
			}	
		}
		
		for(int i= 0; i < 5;i++){		
			for(int j= 0; j < 5;j++){
				System.out.print( "|" + matriz[i][j] + "|");
			}	
			System.out.println();
		}
		
		return matriz;
		
		
		
	}
	
	
	public static String[][] pintarMatriz(HashMap<Integer, Integer> posicoes){
		String[][] matriz = new String[5][5];
		
		
		for(int i= 0; i < 5;i++){
			for(int j= 0; j < 5;j++){
				if (posicoes.containsKey(i) && posicoes.get(i) == j){
					matriz[i][j] = "X";				
				}
				else
				matriz[i][j] = " ";
			}	
		}
		return matriz;
	}
	

}
